///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/
/////////////////////////////////////////////////////////////////// 

#include "BJetCalibrationTool/BJetCalibrationToolBase.h"
#include "PathResolver/PathResolver.h"
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"

#include "xAODMuon/MuonContainer.h"

BJetCalibrationToolBase::BJetCalibrationToolBase(const std::string& name) : asg::AsgTool (name), m_JetAlgo(""), m_Jet_Min_Pt(), m_Jet_Min_Eta(), m_MuonContainer_Name(""), m_Muon_Quality(), m_Muon_Min_Pt(), m_Muon_Jet_dR(), m_doVR()
{ 
  declareProperty("JetCollection",    m_JetAlgo            = "AntiKt4EMPFlow");
  declareProperty("Jet_Min_Pt",       m_Jet_Min_Pt         = 20.);
  declareProperty("Jet_Min_Eta",      m_Jet_Min_Eta        = 2.5);
  declareProperty("MuonContainer",    m_MuonContainer_Name = "Muons");
  declareProperty("MuonQuality",      m_Muon_Quality       = xAOD::Muon::Medium);
  declareProperty("Muon_Min_Pt",      m_Muon_Min_Pt        = 3.);
  declareProperty("Jet_Muon_dR",      m_Muon_Jet_dR        = 0.4);
  declareProperty("doVR",             m_doVR               = true);
  declareProperty("scaleName",        m_scaleName          = "OneMu");
  declareProperty("StoreCorrected4v", m_storeCorrected4v   = false);
}

StatusCode BJetCalibrationToolBase::initialize() {

  ANA_MSG_INFO("Initializing BJetCalibrationToolBase ... "); 
  
  if (m_JetAlgo == "") {
    ANA_MSG_FATAL("BJetCalibrationToolBase::initialize() : Please set JetCollection"); 
    return StatusCode::FAILURE;
  }

  m_muonSelection.setTypeAndName("CP::MuonSelectionTool/MuonSelectionToolForBJetCalibrationTool");
  ANA_CHECK(m_muonSelection.setProperty( "MaxEta", 2.5 ));
  ANA_CHECK(m_muonSelection.setProperty( "MuQuality",(int) m_Muon_Quality));
  ANA_CHECK(m_muonSelection.initialize());
  
  m_muonsReady = false;

  ANA_MSG_INFO("BJetCalibrationToolBase is initialized!");

  return StatusCode::SUCCESS;
  
}

StatusCode BJetCalibrationToolBase::initializeMuonContainer() {
  
  m_muons.clear();
  const xAOD::MuonContainer* MuonContainer = 0;	
  if (evtStore()->retrieve( MuonContainer, m_MuonContainer_Name).isFailure()) {
    ANA_MSG_FATAL("initializeMuonContainer() : Please check MuonContainer name");
    return StatusCode::FAILURE;
  }
  for (auto mu : *MuonContainer) {
    if (mu->pt()*1e-3 > m_Muon_Min_Pt && m_muonSelection->accept(*mu)) {
      m_muons.push_back(mu);
    }
  }
  
  ATH_MSG_DEBUG("Selected " << m_muons.size() << " good muons out of " << MuonContainer->size() << " input ones");
  m_muonsReady = true;
  
  return StatusCode::SUCCESS;
  
}

StatusCode BJetCalibrationToolBase::applyBJetCalibration(xAOD::Jet& /*jet*/) { 
  return StatusCode::SUCCESS; 
}

std::vector< const xAOD::Muon* > BJetCalibrationToolBase::getMuonInJet(xAOD::Jet& jet, std::vector< const xAOD::Muon* > muons) {

  std::vector< const xAOD::Muon* > m_muon_in_jet;

  for (unsigned int i = 0; i<muons.size(); i++) {
    TLorentzVector mu = muons[i]->p4();	
    TLorentzVector j = jet.p4();	
    Double_t dR = j.DeltaR(mu);
			
    if (m_doVR) {
      m_Muon_Jet_dR = std::min(0.4, 0.04 + (10/ (((mu).Pt() * 0.001))));	
    } 		
    if (dR > m_Muon_Jet_dR) continue;
    m_muon_in_jet.push_back(muons[i]);		
  }
  return m_muon_in_jet;
}
