///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// BJetCalibrationTool.cxx
// Source file for class BJetCalibrationTool
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
/////////////////////////////////////////////////////////////////// 


// BJetCalibrationTool includes
#include "BJetCalibrationTool/BJetMuInJetPtRecoTool.h"
#include "PathResolver/PathResolver.h"

#include "xAODMuon/MuonContainer.h"
#include "TH1F.h"
#include "TFile.h"

// Constructors
////////////////

BJetMuInJetPtRecoTool::BJetMuInJetPtRecoTool(const std::string& name) : BJetCalibrationToolBase(name), m_doPtReco(true), m_PtReco(""), m_Semi_Histo_name(""), m_Had_Histo_name("")
{ 
  declareProperty("doPtReco",          m_doPtReco        = true);
  declareProperty("PtRecoFile",        m_PtReco          = "PtReco_Correction_MV2c10_DL1r_05032020.root");
  declareProperty("SemiLeptonicHisto", m_Semi_Histo_name = "Correction_SemiLeptonic_DL1r_ttbar_mean");
  declareProperty("SemiHadronicHisto", m_Had_Histo_name  = "Correction_Hadronic_DL1r_ttbar_mean");
}

StatusCode BJetMuInJetPtRecoTool::initialize() {

  ANA_MSG_INFO("Initializing ..."); 

  ANA_CHECK(BJetCalibrationToolBase::initialize());

  if (m_doPtReco) {
    if (m_PtReco == "") {
      ANA_MSG_FATAL("initialize() : Please set PtReco file name");
      return StatusCode::FAILURE;
    }

    TString filename = PathResolverFindCalibFile("BJetCalibrationTool/"+m_JetAlgo+"_"+m_PtReco);
    m_PtRecoFile = new TFile(filename,"READ");
    if (!m_PtRecoFile) {
      ANA_MSG_FATAL("initialize() : PtReco file is not found" << filename );
      return StatusCode::FAILURE;
    }
    m_Semi_Histo = (TH1F*) m_PtRecoFile->Get(m_Semi_Histo_name.c_str()); 
    m_Had_Histo  = (TH1F*) m_PtRecoFile->Get(m_Had_Histo_name.c_str()); 	
    
    if (m_Semi_Histo == NULL or m_Had_Histo == NULL) {
      ANA_MSG_FATAL("initialize() : Please check the histograms names");
      return StatusCode::FAILURE;
    }
  }
  ANA_MSG_INFO("Initialized!");
  return StatusCode::SUCCESS;
}

StatusCode BJetMuInJetPtRecoTool::applyBJetCalibration(xAOD::Jet& jet) { 

  if (!m_muonsReady) 
  { ANA_CHECK(initializeMuonContainer()); }
  std::vector< const xAOD::Muon* > muons_in_jet = getMuonInJet(jet, m_muons);

  addMuon(jet, muons_in_jet);
 
  if(m_doPtReco)
  { applyPtReco(jet); }

  muons_in_jet.clear();
  return StatusCode::SUCCESS; 
}

void BJetMuInJetPtRecoTool::applyPtReco(xAOD::Jet& jet) {
  static SG::AuxElement::Decorator<Float_t> PtRecoF("PtRecoSF");
  
  Int_t nmu = jet.auxdata<Int_t>("n_muons");
  Float_t PtRecoFactor = 1.;
  xAOD::JetFourMom_t j = jet.jetP4();
  
  if(nmu == 0) {
    PtRecoFactor = m_Had_Histo->Interpolate(log(j.Pt() * 0.001));
  } else if (nmu > 0) {
    j += jet.jetP4(m_scaleName.c_str());
    PtRecoFactor = m_Semi_Histo->Interpolate(log(j.Pt() * 0.001));
  }
  ATH_MSG_DEBUG("ptReco = " << PtRecoFactor << " for jet orig pT = " << jet.pt()*1e-3
		<< " muon-corr pT = " << j.Pt()*1e-3 << " from " << std::min(1,nmu) << " mu param"
		<< " final corrected pT = " << PtRecoFactor*j.Pt()*1e-3);

  PtRecoF(jet) = PtRecoFactor;
  // In this case, OneMu is the corrected 4-vector, not the 4-vector itself
  if (m_storeCorrected4v) {
    j *= PtRecoFactor;
    xAOD::JetFourMom_t new_jet(j.Pt(),j.Eta(),j.Phi(),j.M());
    jet.setJetP4(m_scaleName,new_jet);
  }
}

void BJetMuInJetPtRecoTool::addMuon(xAOD::Jet& jet, std::vector< const xAOD::Muon* > muons) {
  static SG::AuxElement::Decorator<Int_t> NMu("n_muons");	
  
  NMu(jet) = muons.size();	
  
  if (jet.auxdata<Int_t>("n_muons") == 0) {
    jet.setJetP4(m_scaleName.c_str(),xAOD::JetFourMom_t(0,0,0,0));
    return;
  } else if(jet.auxdata<Int_t>("n_muons") >= 1) {
    const xAOD::Muon* muon = getMuon(muons);
    
    TLorentzVector mu = muon->p4();
    float eLoss = 0.0;
    muon->parameter(eLoss,xAOD::Muon::EnergyLoss);
    double theta=mu.Theta();
    double phi=mu.Phi();
    double eLossX=eLoss*sin(theta)*cos(phi);
    double eLossY=eLoss*sin(theta)*sin(phi);
    double eLossZ=eLoss*cos(theta);
    TLorentzVector Loss = TLorentzVector(eLossX,eLossY,eLossZ,eLoss);
    
    TLorentzVector j = -Loss+mu;
    xAOD::JetFourMom_t new_jet(j.Pt(),j.Eta(),j.Phi(),j.M());
    
    ANA_MSG_DEBUG("Storing muon correction pT = " << j.Pt() << " from which " << eLoss*sin(theta) << " is from eloss");
    
    jet.setJetP4(m_scaleName.c_str(),new_jet);
    
    return;		
  }
}

const xAOD::Muon* BJetMuInJetPtRecoTool::getMuon(std::vector< const xAOD::Muon* > muons) {

  Int_t muon_size = muons.size();
  Double_t max_muon_pt = muons[0]->pt();
  Int_t index = 0;

  for (Int_t i = 0; i<muon_size; i++) {
    if (max_muon_pt < muons[i]->pt()) {
      max_muon_pt = muons[i]->pt();
      index = i;
    }
  }
  return muons[index];

}
