#include <GaudiKernel/DeclareFactoryEntries.h>

#include <BJetCalibrationTool/BJetCalibrationTool.h>
#include <BJetCalibrationTool/BJetRegressionTool.h>

DECLARE_ALGORITHM_FACTORY (BJetCalibrationTool)
DECLARE_ALGORITHM_FACTORY (BJetRegressionTool)

